Source: bruteforce-salted-openssl
Section: utils
Priority: optional
Maintainer: Debian Security Tools <team+pkg-security@tracker.debian.org>
Uploaders: Joao Eriberto Mota Filho <eriberto@debian.org>
Build-Depends: debhelper (>= 11), libssl-dev
Standards-Version: 4.2.1
Homepage: https://github.com/glv2/bruteforce-salted-openssl
Vcs-Git: https://salsa.debian.org/pkg-security-team/bruteforce-salted-openssl.git
Vcs-Browser: https://salsa.debian.org/pkg-security-team/bruteforce-salted-openssl

Package: bruteforce-salted-openssl
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: try to find the passphrase for files encrypted with OpenSSL
 bruteforce-salted-openssl try to find the passphrase or password of a file
 that was encrypted with the openssl command. It can be used in two ways:
 .
    - Try all possible passwords given a charset.
    - Try all passwords in a file (dictionary).
 .
 bruteforce-salted-openssl have the following features:
 .
    - You can specify the number of threads to use when cracking a file.
    - The program should be able to use all the digests and symmetric ciphers
      available with the OpenSSL libraries installed on your system.
    - Sending a USR1 signal to a running bruteforce-salted-openssl process
      makes it print progress and continue.
    - There are an exhaustive mode and a dictionary mode.
 .
 In the exhaustive mode the program tries to decrypt the file by trying all
 possible passwords. It is especially useful if you know something about the
 password (i.e. you forgot a part of your password but still remember most of
 it). Finding the password of the file without knowing anything about it would
 take way too much time (unless the password is really short and/or weak).
 There are some command line options to specify:
 .
    - The minimum password length to try.
    - The maximum password length to try.
    - The beginning of the password.
    - The end of the password.
    - The character set to use (among the characters of the current locale).
 .
 In dictionary mode the program tries to decrypt the file by trying all the
 passwords contained in a file. The file must have one password per line.
 .
 This package is useful for security, pentests and forensics investigations.
